package com.miniproject.annocr;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.holoeverywhere.app.Application;
import org.holoeverywhere.widget.Toast;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.util.Log;

public class Utility extends Application
{
	public static final boolean internetAvailable( Context context )
	{
		ConnectivityManager cm = (ConnectivityManager) context
				.getApplicationContext().getSystemService( Context.CONNECTIVITY_SERVICE );

		if( cm.getActiveNetworkInfo() != null
				&& cm.getActiveNetworkInfo().isAvailable()
				&& cm.getActiveNetworkInfo().isConnected() )
		{
			return true;
		}
		return false;
	}

	public static final Bitmap decodeFile( String filePath )
	{
		Bitmap bitmap = null;
		// Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeFile( filePath, o );

		// The new size we want to scale to
		final int REQUIRED_SIZE = 1024;

		// Find the correct scale value. It should be the power of 2.
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while( true )
		{
			if( width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE )
				break;
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		// Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		bitmap = BitmapFactory.decodeFile( filePath, o2 );
		return bitmap;
	}

	public static final String saveToDisk( final Bitmap bitmap,
			final Context context )
	{
		final int BUFFER_SIZE = 1024 * 8;

		String filename = generateFileName();
		if( filename == null ){
			Toast.makeText( context,
					"Couldn't create directory to save images!",
					Toast.LENGTH_LONG ).show();
		}
		File imageFile = new File( filename );

		try
		{
			final FileOutputStream fos = new FileOutputStream( imageFile );
			final BufferedOutputStream bos = new BufferedOutputStream( fos,	BUFFER_SIZE );
			bitmap.compress( CompressFormat.JPEG, /** quality **/ 60, bos );
			bos.flush();
			bos.close();
			fos.close();
			return filename;
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		return null;
	}

	public static final String generateFileName()
	{
		File sdDirectory = Environment
				.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES );
		File tempImageDir = new File( sdDirectory, "ANNOCR" );
		if( !tempImageDir.exists() && !tempImageDir.mkdirs() )
		{
			Log.e( "ANNOCR", "Couldn't create directory to save images!" );
			return null;
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyymmddhhmmss" );
		String date = dateFormat.format( new Date() );
		String photoFile = "Picture_ANNOCR_" + date + ".jpg";
		return tempImageDir.getPath() + File.separator + photoFile;
	}
}
