package com.miniproject.annocr;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.holoeverywhere.widget.Toast;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.commonsware.cwac.wakeful.WakefulIntentService;
import com.miniproject.annocr.db.UploadConstants;
import com.miniproject.annocr.db.UploadDataHelper;
import com.miniproject.annocr.preferences.ANNOCRPreferenceManager;

public class MiniProjectUploadService extends WakefulIntentService implements
		UploadConstants
{
	private static final int BUFFER_SIZE = 1024 * 8;
	private static final String LOG_TAG = "ANNOCR Upload Service";
	private Socket m_socket = null;
	private NotificationManager m_notiManager;
	public static final String BROADCAST_STATECHANGE = "serviceStateChange";
	public static final String BROADCAST_UPLOAD = "uploadingImage";

	private static boolean s_running = false;
	public static final boolean isRunnning()
	{
		return s_running;
	}

	public MiniProjectUploadService()
	{
		super( "ANN-OCR Upload Service" );
		s_running = true;
	}

	@Override
	public void onCreate()
	{
		super.onCreate();
		Log.v( LOG_TAG, "Service starting..." );
		LocalBroadcastManager.getInstance( this ).sendBroadcast( new Intent( BROADCAST_STATECHANGE ) );
        m_notiManager = (NotificationManager) getSystemService( Context.NOTIFICATION_SERVICE );
	}

	public static void schedule( final Context context, long timeMillis )
	{
		Context appContext = context.getApplicationContext();
		AlarmManager am = (AlarmManager) context.getSystemService( Context.ALARM_SERVICE );
   		Intent newintent = new Intent( appContext, ANNOCRBroadcastReceiver.class );
   		PendingIntent pendingIntent = PendingIntent.getBroadcast( appContext, 1234,
				    newintent, PendingIntent.FLAG_ONE_SHOT );
   		am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + timeMillis, pendingIntent );
	}

	@Override
	protected void doWakefulWork( Intent intent )
	{
		// TODO run in foreground
		//Bundle bundle = ( intent != null ? intent.getExtras() : null );
		//if( bundle == null )
		// 	return;
		if( !Utility.internetAvailable( this ) )
		{
			Log.v( LOG_TAG, "No internet available, halting upload" );
			Toast.makeText( getApplicationContext(), R.string.uploadservice_error_nointernet, Toast.LENGTH_SHORT ).show();
			// TODO add an network change receiver, reschedule
		}
		SQLiteDatabase db = UploadDataHelper.instance( this )
				.getWritableDatabase();
		Cursor cursor = db.query( TABLE_NAME_UPLOADQUEUE, new String[] { _ID,
				FILE_NAME, FILE_PATH }, SENT + " = 0 AND " + FAILED + " = 0 ", null, null, null, TIMESTAMP );
		if( cursor.getCount() < 0 )
		{
			cursor.close();
			return;
		}
		try
		{
			InetSocketAddress socketAddress = ANNOCRPreferenceManager.instance( this ).serverAddress();
			if( socketAddress == null )
			{
				// TODO send notification
				stopForeground( true );
				stopSelf();
				return;
			}
			Log.v( LOG_TAG, "Connecting to server" + socketAddress.toString() );
			m_socket = new Socket( socketAddress.getAddress(), socketAddress.getPort() );
		}
		catch( IOException e )
		{
			e.printStackTrace();
			Log.e( LOG_TAG, "Failed to create socket" );
			stopForeground( true );
			stopSelf();
			// TODO deliver notification
			return;
		}
		while( cursor.moveToNext() )
		{
			String filePath = cursor.getString( cursor.getColumnIndex( FILE_PATH ) );
			long fileID = cursor.getLong( cursor.getColumnIndex( _ID ) );
			String fileName = cursor.getString( cursor.getColumnIndex( FILE_NAME ) );
			Log.v( LOG_TAG, "Uploading: " + filePath );
			String result = null;
			try
			{
				result = uploadImage( filePath );
				if( result == null )
				{
					Log.e(LOG_TAG,"Result is null");
					continue;
				}
				ContentValues cv = new ContentValues();
				cv.put( SENT, 1 );
				cv.put( RESULT, result );
				db.update( TABLE_NAME_UPLOADQUEUE, cv, _ID + " = " + fileID, null );
				// TODO display notification
				Log.v( LOG_TAG, "File " + fileName + "successfully transcribed!" );
				LocalBroadcastManager.getInstance( this ).sendBroadcast(
						new Intent( UploadDataHelper.ACTION_CHANGED ) );
			}
			catch( UnknownHostException e )
			{
				// TODO notify wrong settings
				e.printStackTrace();
				break;
			}
			catch( FileNotFoundException e )
			{
				// TODO notify image deleted
				ContentValues cv = new ContentValues();
				cv.put( FAILED, 1 );
				db.update( TABLE_NAME_UPLOADQUEUE, cv, _ID + " = " + fileID, null );
				e.printStackTrace();
			}
			catch( IOException e )
			{
				// TODO what?
				e.printStackTrace();
			}
		}
		cursor.close();
		try
		{
			m_socket.close();
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
		schedule( this, 3600 * 1000 * 2 );
	}

	private String uploadImage( String imagePath ) throws IOException, UnknownHostException, FileNotFoundException
	{
		String result = "";
		File imageFile = new File( imagePath );
		FileInputStream fis = new FileInputStream( imageFile );
		OutputStream os = m_socket.getOutputStream();
		byte buffer[] = new byte[1024];
		int read_count = 0;
		while( ( read_count = fis.read( buffer, 0, buffer.length ) ) != -1 )
			os.write( buffer, 0, read_count );

		Log.v( LOG_TAG, "Written image to stream, flushing..." );
		os.flush();
		Log.v( LOG_TAG, "Flushed stream, closing output stream..." );
		try
		{
			// os.close();
			m_socket.shutdownOutput();
		}
		catch( SocketException e )
		{
			e.printStackTrace();
		}
		// read result
		BufferedReader in = new BufferedReader( new InputStreamReader(
				m_socket.getInputStream() ) );
		Log.v( LOG_TAG, "Reading server response" );
		in.ready();
		Log.v( LOG_TAG, "Awaiting server response" );
		String temp;
		while( ( temp = in.readLine() ) != null )
			result += temp;
		Log.v( LOG_TAG, "Done" );
		Log.v( LOG_TAG, "Result: " + result );
		Log.v( LOG_TAG, "Shutting i/o..." );
		try
		{
			m_socket.shutdownInput();
		}
		catch( SocketException e )
		{
			e.printStackTrace();
		}
		Log.v( LOG_TAG, result );
		return result;
	}

	private void notify( final String title, final String text, final int drawableId )
	{
		/*
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder( getApplicationContext() );
        builder.setContentTitle( getString(R.string.app_name) )
               .setContentText( text )
               // TODO .setSmallIcon( R.drawable.ic_notification )
               .setContentIntent( gpsPendingIntent );
                       notifyManager.notify( 0, builder.build() );*/
	}

	@Override
	public void onDestroy()
	{
		s_running = false;
		LocalBroadcastManager.getInstance(this).sendBroadcast( new Intent( BROADCAST_STATECHANGE ) );
		super.onDestroy();
	}
}
