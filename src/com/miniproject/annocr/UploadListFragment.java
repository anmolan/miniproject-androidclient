package com.miniproject.annocr;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.ListFragment;
import org.holoeverywhere.widget.CheckedTextView;
import org.holoeverywhere.widget.ListView;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.commonsware.cwac.loaderex.acl.SQLiteCursorLoader;
import com.miniproject.annocr.db.UploadConstants;
import com.miniproject.annocr.db.UploadDataHelper;

public class UploadListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>, UploadConstants
{

	private SimpleCursorAdapter m_adapter;
	private SQLiteCursorLoader m_loader;
	private String m_filter;
	private SearchView m_searchView;
	private BroadcastReceiver m_receiver;
	private BroadcastReceiver m_uploadEventReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive( Context context, Intent intent )
		{
			// TODO get id of image being uploaded, indicate
		}
	};

	@Override
	public void onViewCreated( View view, Bundle savedInstanceState )
	{
		super.onViewCreated( view, savedInstanceState );
		LocalBroadcastManager.getInstance( getActivity() ).registerReceiver( m_uploadEventReceiver,
			      new IntentFilter( MiniProjectUploadService.BROADCAST_UPLOAD ) );
        setEmptyText( getString(R.string.EmptyListText) );
        m_adapter = new SimpleCursorAdapter( getActivity(),
        		R.layout.fragment_uploadlist_upload_item, null,
    			new String[]{ FILE_NAME, FILE_PATH }, new int[]{ R.id.upload_item_name, R.id.upload_item_thumbnail }, 0 );
        m_receiver = new BroadcastReceiver()
		{
			@Override
			public void onReceive( Context context, Intent intent )
			{
				setListShown( false );
		        getLoaderManager().restartLoader( 0, null, UploadListFragment.this );
			}
		};
        m_adapter.setViewBinder( new SimpleCursorAdapter.ViewBinder()
		{
			@Override
    		public boolean setViewValue( View view, Cursor c, int columnIndex )
    		{
    			switch( view.getId() )
    			{
    			case R.id.upload_item_name:
    				CheckedTextView ctv = (CheckedTextView) view;
    				ctv.setText( c.getString( c.getColumnIndex( FILE_NAME ) ) );
    				if( c.getInt( c.getColumnIndex( SENT ) ) == 1 )
    				{
    					ctv.setPaintFlags( ctv.getPaintFlags()
    							| Paint.STRIKE_THRU_TEXT_FLAG );
    					ctv.setChecked( true );
    				}
    				else
    				{
    					ctv.setPaintFlags( ctv.getPaintFlags()
    							& ( ~Paint.STRIKE_THRU_TEXT_FLAG ) );
    					ctv.setChecked( false );
    				}
    				return true;

    			case R.id.upload_item_thumbnail:
    				String filePath = c.getString( c.getColumnIndex( FILE_PATH ) );
    				ImageView preview = (ImageView) view;
    				preview.setImageDrawable( new BitmapDrawable( getResources(), Utility.decodeFile( filePath ) ) );
    				return true;
    			}

    			return false;
    		}
		} );
        setListShown(false);
        setListAdapter( m_adapter );
		setHasOptionsMenu( true );
        getLoaderManager().initLoader( 0, null, this );
	}

	@Override
	public void onStart()
	{
		super.onStart();
		LocalBroadcastManager.getInstance( getActivity() ).registerReceiver(
				m_receiver,
				new IntentFilter( UploadDataHelper.ACTION_CHANGED ) );
	}

	@Override
	public void onStop()
	{
		try
		{
			LocalBroadcastManager.getInstance( getActivity() ).unregisterReceiver( m_receiver );
		}
		catch( IllegalArgumentException e )
		{
			e.printStackTrace();
		}
		super.onStop();
	}

	@Override
	public void onCreateOptionsMenu( Menu menu, MenuInflater inflater )
	{
		super.onCreateOptionsMenu( menu, inflater );
		m_searchView = new SearchView( ((Activity) getActivity())
				.getSupportActionBar().getThemedContext() );
		m_searchView.setQueryHint( "Search" );

		MenuItem item = menu.add( Menu.NONE, Menu.NONE, 1, R.string.menu_search )
				.setIcon( R.drawable.ic_menu_search_holo_light );
		MenuItemCompat.setActionView( item, m_searchView );
		MenuItemCompat.setShowAsAction( item, MenuItemCompat.SHOW_AS_ACTION_ALWAYS | MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW );

		m_searchView.setOnQueryTextListener( new OnQueryTextListener()
		{
			@Override
			public boolean onQueryTextChange( String newText )
			{
				m_filter = newText;
				getLoaderManager().restartLoader( 0, null, UploadListFragment.this );
				return false;
			}

			@Override
			public boolean onQueryTextSubmit( String query )
			{
				InputMethodManager imm = (InputMethodManager) getActivity()
						.getSystemService( Context.INPUT_METHOD_SERVICE );
				imm.hideSoftInputFromWindow( m_searchView.getWindowToken(), 0 );
				return false;
			}
		} );

	}

	@Override
	public Loader<Cursor> onCreateLoader( int arg0, Bundle arg1 )
	{
		String query = "SELECT * FROM " + TABLE_NAME_UPLOADQUEUE;
		if( m_filter != null && m_filter.length() > 0 )
		{
			query += " WHERE UPPER( " + FILE_NAME + " ) like '%" + m_filter.toUpperCase() + "%'";
		}
		query += " ORDER BY " + TIMESTAMP + " ASC";
		m_loader = new SQLiteCursorLoader( getActivity(), UploadDataHelper.instance( getActivity() ), query, null );
		return m_loader;
	}

	@Override
	public void onLoadFinished( Loader<Cursor> loader, Cursor cursor )
	{
		m_adapter.changeCursor( cursor );
        if( isResumed() )
            setListShown( true );
        else
            setListShownNoAnimation( true );
	}

	@Override
	public void onLoaderReset( Loader<Cursor> loader )
	{
        m_adapter.changeCursor( null );
    }

	@Override
	public void onListItemClick( ListView l, View v, int position, final long id )
	{
		Cursor c = m_adapter.getCursor();
		if( c == null )
			return;
		String resultText = c.getString( c.getColumnIndex( RESULT ) );
		if( resultText == null || resultText.length() == 0 )
		{
			resultText = getString( R.string.dialog_message_NoResultErrorMessage );
		}
		new AlertDialog.Builder( getActivity() )
				.setTitle( getString( R.string.dialog_title_Results ) )
				.setMessage( resultText )
				.setCancelable( true )
				.setNeutralButton( getText( R.string.dialog_button_Okay ),
						new OnClickListener()
						{
							@Override
							public void onClick( DialogInterface dialog, int which )
							{
								dialog.dismiss();
							}
						} )
				.setNegativeButton( R.string.Delete,
						new OnClickListener()
						{
							@Override
							public void onClick( DialogInterface dialog, int which )
							{
								m_loader.delete( TABLE_NAME_UPLOADQUEUE, _ID + " = " + id, null );
								dialog.dismiss();
							}
						} )
						.create().show();
		super.onListItemClick( l, v, position, id );
	}

	@Override
	public void setUserVisibleHint( boolean isVisibleToUser )
	{
		super.setUserVisibleHint( isVisibleToUser );
		if( this.isVisible() )
		{
			if( !isVisibleToUser )
			{
				m_filter = null;
			}
		}

	}

}
