package com.miniproject.annocr;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import com.miniproject.annocr.preferences.ANNOCRPreferenceActivity;
import com.miniproject.annocr.swipeytabs.SwipeyActivity;

public class MiniProjectMain extends SwipeyActivity
{
	private final BroadcastReceiver m_serviceStateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive( Context context, Intent intent )
		{
			setProgressBarIndeterminateVisibility( MiniProjectUploadService.isRunnning() );
			supportInvalidateOptionsMenu();
		}
	};

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		MiniProjectUploadService.schedule( this, 0 );
		// MUST be called before setting layout
		supportRequestWindowFeature( Window.FEATURE_INDETERMINATE_PROGRESS );
		super.onCreate( savedInstanceState );
		addTab( new MainFragment(), "Main" ); // TODO hardcoded strings
		addTab( new UploadListFragment(), "Queue" );
	}

	@Override
	public boolean onCreateOptionsMenu( Menu menu )
	{
		getMenuInflater().inflate( R.menu.activity_main, menu );
		MenuItem uploadItem = menu.findItem( R.id.menu_upload );
		uploadItem.setVisible( !MiniProjectUploadService.isRunnning() );
		MenuItemCompat.setShowAsAction( uploadItem, MenuItemCompat.SHOW_AS_ACTION_IF_ROOM );
		return true;
	}

	@Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        switch( item.getItemId() )
        {
	        case R.id.menu_settings:
	        	startActivity( new Intent( this, ANNOCRPreferenceActivity.class ) );
	        	break;
	        case R.id.menu_upload:
	        	MiniProjectUploadService.schedule( this, 0 );
	        	break;
        }
		return super.onOptionsItemSelected( item );
    }

	@Override
	protected void onResume()
	{
		super.onResume();
		LocalBroadcastManager.getInstance( this ).registerReceiver( m_serviceStateReceiver, new IntentFilter( MiniProjectUploadService.BROADCAST_STATECHANGE ) );
		setProgressBarIndeterminateVisibility( MiniProjectUploadService.isRunnning() );
	}

	@Override
	protected void onPause()
	{
		LocalBroadcastManager.getInstance( this ).unregisterReceiver( m_serviceStateReceiver );
		super.onPause();
	}

}
