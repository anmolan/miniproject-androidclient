package com.miniproject.annocr.db;

import android.provider.BaseColumns;

public interface UploadConstants extends BaseColumns
{
	public static final String TABLE_NAME_UPLOADQUEUE = "upload_data_table";
	public static final String FILE_NAME = "file_name";
	public static final String FILE_PATH = "file_path";
	public static final String RESULT = "result";
	public static final String TIMESTAMP = "timestamp";
	public static final String SENT = "sent";
	public static final String FAILED = "failed";
}
