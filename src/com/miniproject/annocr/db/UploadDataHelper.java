package com.miniproject.annocr.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UploadDataHelper extends SQLiteOpenHelper implements UploadConstants
{
	private static final String DATABASE_NAME= "packages.db";
	private static final int DATABASE_VERSION= 1;
	private static UploadDataHelper s_instance;

	public static final String ACTION_CHANGED = "sqlitedb_changed";

	public static UploadDataHelper instance( final Context context )
	{
		if( s_instance == null )
			s_instance = new UploadDataHelper( context.getApplicationContext() );
		return s_instance;
	}

	private UploadDataHelper( final Context context )
	{
		super( context, DATABASE_NAME, null, DATABASE_VERSION );
	}

	@Override
	public void onCreate( SQLiteDatabase db )
	{
		db.execSQL( "CREATE TABLE " + UploadConstants.TABLE_NAME_UPLOADQUEUE + " (" +
					_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					UploadConstants.FILE_NAME + " TEXT NOT NULL DEFAULT 'File', " +
					UploadConstants.FILE_PATH + " TEXT NOT NULL UNIQUE, " +
					UploadConstants.RESULT + " TEXT, " +
					UploadConstants.SENT + " INTEGER NOT NULL DEFAULT 0, " +
					UploadConstants.FAILED + " INTEGER NOT NULL DEFAULT 0, " +
					UploadConstants.TIMESTAMP + " INTEGER NOT NULL );" );
	}

	@Override
	public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion )
	{
	}
}
