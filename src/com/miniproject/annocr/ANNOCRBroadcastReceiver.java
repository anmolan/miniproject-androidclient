package com.miniproject.annocr;

import com.commonsware.cwac.wakeful.WakefulIntentService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

public class ANNOCRBroadcastReceiver extends BroadcastReceiver
{
	private static final String LOG_TAG = "ANNOCR Receiver";
	@Override
	public void onReceive( Context context, Intent intent )
	{
		final String action = intent.getAction();
		if( action != null )
		{
			if( action.equals( ConnectivityManager.CONNECTIVITY_ACTION ))
			{
				if( !Utility.internetAvailable( context ) )
					return;
			}
		}
		WakefulIntentService.sendWakefulWork( context, MiniProjectUploadService.class );
	}
}
