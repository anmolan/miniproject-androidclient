package com.miniproject.annocr.preferences;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

import com.miniproject.annocr.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.PreferenceManager;
import android.util.Log;

public class ANNOCRPreferenceManager
{
	private SharedPreferences m_sharedPreferences;
	private Context m_context;
	private static final String LOG_TAG = "ANNOCR Preference Manager";

	private static ANNOCRPreferenceManager s_instance;

	public static final String ACTION_PREFERENCE_CHANGED = "preferences_changed";

	public static ANNOCRPreferenceManager instance( Context context )
	{
		if( s_instance == null )
			s_instance = new ANNOCRPreferenceManager( context.getApplicationContext() );
		return s_instance;
	}

	private ANNOCRPreferenceManager( Context context )
	{
		m_context = context;
		m_sharedPreferences = PreferenceManager.getDefaultSharedPreferences( context );
	}

	public boolean runInForeground()
	{
		return m_sharedPreferences.getBoolean( getString(R.string.bool_run_in_foreground), true );
	}

	public boolean enabled()
	{
		return m_sharedPreferences.getBoolean( getString(R.string.bool_enable_parent) , true );
	}

	public boolean enableBackgroundUpload()
	{
		return m_sharedPreferences.getBoolean( getString(R.string.bool_enable_bgupload) , true );
	}

	public boolean enableUploadOnMobileData()
	{
		return m_sharedPreferences.getBoolean( getString(R.string.bool_enable_dataupload) , true );
	}

	public InetSocketAddress serverAddress()
	{
		String ipString = m_sharedPreferences.getString( getString(R.string.ipaddress_serverip) , null );
		if( ipString == null )
			return null;
		InetSocketAddress socketAddress = null;
		try
		{
			InetAddress address = null;
			address = InetAddress.getByName( ipString );
			int port = Integer.parseInt( m_sharedPreferences.getString( getString(R.string.ipaddress_port), "-1" ) );
			if( port == -1 )
			{
				Log.w( LOG_TAG, "No port specified, defaulting to 1234" );
				port = 1234;
			}
			socketAddress = new InetSocketAddress( address, port );
		}
		catch( ClassCastException e )
		{
			Log.e( LOG_TAG, e.toString() );
			e.printStackTrace();
		}
		catch( NumberFormatException e )
		{
			Log.e( LOG_TAG, "Invalid IP" );
			e.printStackTrace();
		}
		catch( UnknownHostException e )
		{
			Log.e( LOG_TAG, "Invalid IP length" );
			e.printStackTrace();
		}
		return socketAddress;
	}

	public void registerListener( OnSharedPreferenceChangeListener listener )
	{
		m_sharedPreferences.registerOnSharedPreferenceChangeListener( listener );
	}

	public void unregisterListener( OnSharedPreferenceChangeListener listener )
	{
		m_sharedPreferences.unregisterOnSharedPreferenceChangeListener( listener );
	}

	private String getString( int id )
	{
		return m_context.getString( id );
	}
}
