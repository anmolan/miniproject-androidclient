package com.miniproject.annocr.preferences;

import org.holoeverywhere.preference.PreferenceFragment;

import com.miniproject.annocr.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

public class ANNOCRPreferenceFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener
{

	boolean m_preferencesChanged = false;

	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		addPreferencesFromResource( R.xml.preferences );
	}

	@Override
	public void setUserVisibleHint( boolean isVisibleToUser )
	{
		super.setUserVisibleHint( isVisibleToUser );
		if( this.isVisible() )
		{
			if( !isVisibleToUser )
			{
				// broadcast changes
				if( m_preferencesChanged )
				{
					m_preferencesChanged = false;
					LocalBroadcastManager
							.getInstance( getActivity() )
							.sendBroadcast(	new Intent(	ANNOCRPreferenceManager.ACTION_PREFERENCE_CHANGED ) );
				}
			}
		}
	}

	@Override
	public void onPause()
	{
		ANNOCRPreferenceManager.instance( getActivity() ).unregisterListener( this );
		super.onPause();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		ANNOCRPreferenceManager.instance( getActivity() ).registerListener( this );
	}

	@Override
	public void onSharedPreferenceChanged( SharedPreferences sharedPreferences, String key )
	{
		m_preferencesChanged = true;
	}

}
