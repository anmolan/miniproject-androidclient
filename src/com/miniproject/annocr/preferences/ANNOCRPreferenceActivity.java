package com.miniproject.annocr.preferences;

import org.holoeverywhere.app.Activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

public class ANNOCRPreferenceActivity extends Activity
{

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		FragmentManager fm = getSupportFragmentManager();
		fm.beginTransaction().replace( android.R.id.content, new ANNOCRPreferenceFragment() ).commit();
	}

}
