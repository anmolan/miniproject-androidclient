package com.miniproject.annocr;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.Toast;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.commonsware.cwac.wakeful.WakefulIntentService;
import com.miniproject.annocr.db.UploadConstants;
import com.miniproject.annocr.db.UploadDataHelper;

// TODO hardcoded strings
public class MainFragment extends Fragment
{
	private static final int PICK_IMAGE = 1;
	private static final int TAKE_PIC = 2;
	private ImageView m_imageView;
	private Bitmap m_bitmap = null;
	private String m_filePath = null;
	private static final String LOG_TAG = "MainFragment";

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState )
	{
		setHasOptionsMenu( true );
		final View view = inflater.inflate( R.layout.fragment_imageupload, container, false );
		m_imageView = (ImageView) view.findViewById( R.id.imageview_preview );
		final Button upload = (Button) view.findViewById( R.id.button_upload );
		final EditText et_fileName = (EditText) view.findViewById( R.id.et_fileName );
		upload.setOnClickListener( new View.OnClickListener()
		{
			public void onClick( final View view )
			{
				if( m_bitmap == null )
				{
					Toast.makeText( MainFragment.this.getActivity(),
							"Please select image", Toast.LENGTH_SHORT ).show();
				}
				else
				{
					final Context context = getActivity().getApplicationContext();
					final ContentValues cv = new ContentValues();
					if( m_filePath == null )
					{
						String filePath = Utility.saveToDisk( m_bitmap, context );
						if( filePath == null )
						{
							Toast.makeText( context, "Error saving file to disk", Toast.LENGTH_SHORT );
							return;
						}
						m_filePath = filePath;
					}
					String fileName = et_fileName.getText().toString();
					if( fileName != null )
						cv.put( UploadConstants.FILE_NAME, fileName );
					cv.put( UploadConstants.FILE_PATH, m_filePath );
					cv.put( UploadConstants.TIMESTAMP, System.currentTimeMillis() );
					final ProgressDialog dialog = new ProgressDialog( getActivity() );
					dialog.setCancelable( false );
					dialog.setTitle( "Please wait..." );
					dialog.setMessage( "Readying image for upload..." );
					dialog.show();
					new Thread( new Runnable()
					{
						@Override
						public void run()
						{ 
							if( UploadDataHelper.instance( context ).getWritableDatabase().insert( UploadConstants.TABLE_NAME_UPLOADQUEUE, null, cv ) != -1 )
								LocalBroadcastManager.getInstance( context ).sendBroadcast( new Intent( UploadDataHelper.ACTION_CHANGED ) );
							else
								Log.e( LOG_TAG, "Failed to write entry to db" );
							WakefulIntentService.sendWakefulWork( context, MiniProjectUploadService.class );
							dialog.dismiss();
						}
					} ).start();
					m_bitmap = null;
					m_filePath = null;
				}
			}
		} );
		return view;
	}

	@Override
	public void onCreateOptionsMenu( Menu menu, MenuInflater inflater )
	{
		inflater.inflate( R.menu.imageupload_menu, menu );
	}

	@Override
	public boolean onOptionsItemSelected( MenuItem item )
	{
		switch( item.getItemId() )
		{
		case R.id.menu_gallery:
			try
			{
				Intent intent = new Intent();
				intent.setType( "image/*" );
				intent.setAction( Intent.ACTION_GET_CONTENT );
				startActivityForResult(
						Intent.createChooser( intent, "Select Picture" ),
						PICK_IMAGE );
			}
			catch( Exception e )
			{
				Toast.makeText( MainFragment.this.getActivity(),
						"Exception: " + e, Toast.LENGTH_LONG ).show();
				Log.e( e.getClass().getName(), e.getMessage(), e );
			}
			return true;

		case R.id.menu_camera:
			Intent cameraIntent = new Intent( android.provider.MediaStore.ACTION_IMAGE_CAPTURE );
			startActivityForResult( cameraIntent, TAKE_PIC );
			return true;

		default:
			return super.onOptionsItemSelected( item );
		}
	}

	@Override
	public void onActivityResult( int requestCode, int resultCode, Intent data )
	{
		if( resultCode != Activity.RESULT_OK )
			return;

		switch( requestCode )
		{
		case TAKE_PIC:
		{
			if( data.getExtras() != null )
			{
				m_filePath = null;
				m_bitmap = (Bitmap) data.getExtras().get( "data" );
				m_imageView.setImageBitmap( m_bitmap );
			}
			break;
		}

		case PICK_IMAGE:
		{
			Uri selectedImageUri = data.getData();
			try
			{
				// OI FILE Manager
				String filemanagerstring = selectedImageUri.getPath();
				// MEDIA GALLERY
				String selectedImagePath = null;
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().managedQuery( selectedImageUri, projection, null, null, null );
				if( cursor != null )
				{
					int column_index = cursor.getColumnIndexOrThrow( MediaStore.Images.Media.DATA );
					cursor.moveToFirst();
					selectedImagePath = cursor.getString( column_index );
				}

				if( selectedImagePath != null )
					m_filePath = selectedImagePath;
				else if( filemanagerstring != null )
					m_filePath = filemanagerstring;
				else
				{
					Toast.makeText( getActivity(), "Unknown path",
							Toast.LENGTH_LONG ).show();
					Log.e( "Bitmap", "Unknown path" );
				}

				if( m_filePath != null )
				{
					m_bitmap = Utility.decodeFile( m_filePath );
					m_imageView.setImageBitmap( m_bitmap );
				}
				else
				{
					m_bitmap = null;
				}
			}
			catch( Exception e )
			{
				Toast.makeText( getActivity(), "Internal error",
						Toast.LENGTH_LONG ).show();
				Log.e( e.getClass().getName(), e.getMessage(), e );
			}
			break;
		}

		default:
			break;
		}
	}

}
